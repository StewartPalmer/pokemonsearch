# PokemonSearch

TrueLayer Full Stack Enginer Challenge Notes
 
TimeSpent: 5.5 Hours
 
## Prerequisites  
 
* Node v16^
* Npm v8.1^
* dotNet6.0
* Visual Studio Code
* Visual Studio 2022
* dotnet Cli (if you wish you run via command line)
 
 
Before we go any further, I just wanted to say I had a lot of fun building this.
 
 
## To Run:
 
* With all the above, Open the Solution in visual studio 2022 and run it form there.
* You can also run is using the command 'dotnet run' in the root directory and visting: https://localhost:7101/


* You may need to cd in to the ClientApp and npm install if your visual studio dose not have the required permissions to do,

 
## Overview
 
* The solution is a very simple React (v18) Single Page Application, writen in Typescript. The Backend Api Written in dotNet version 6.  The front end is styled using Tailwind. The application will allow you to search a Pokemon's name, and returns the name, sprite and Shakespearian description.
* Unit tests for all services in the backend are including
* Simple react jest test are including to check rendering is working as expected
* A set of Cyprus test following basic user experience.
* I've tried to keep accessibility in mind, although I did miss some aria labels out.
 
 
## Design Decision
* I chose not to abstract out Models in to class libraries due to the simplest of the application
* I chose not to use react router, again just to due the simplicity of the application although I did consider it.
* I could probably do more to tidy up my class usage in Tailwind
 
 
 
## Additional Notes
* I started working on dockeriztion however, my home computer does not have virtualisation currently enabled. Only my work machines have it enabled. So I was unable to complete this nice to have, the file is there however it is untested.
* I have included a Funtranslations api  key in the solution as the rate limt becomes an issue when running all tests.


## Other Commands:
* To Run Cyrpus (and its tests) : 'npx cypress run'
* To Test The React Jest Tests: 'npm run test'
* Unit Tests can be ran form within Visual Studio or by running 'dotnet run' 