using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestPlatform.TestHost;
using NUnit.Framework;
using PokemonSearch.Controllers;
using PokemonSearch.Models.ResponseEntites;
using PokemonSearch.Services;
using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace PokemonSearch.Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {

            var builder = new ConfigurationBuilder()
              .SetBasePath(Directory.GetCurrentDirectory())
              .AddJsonFile("appsettings.json", false, true);

            Configuration = builder.Build();

            var serviceCollection = new ServiceCollection(); 

            serviceCollection.AddSingleton<IPokeapiService, PokeapiService>();

            serviceCollection.AddSingleton<IShakespeareTranslatorService, ShakespeareTranslatorService>();

            serviceCollection.AddScoped<IPokemonSearchService, PokemonSearchService>();

            serviceCollection.AddHttpClient<IPokeapiService, PokeapiService>(client =>
            {
                client.BaseAddress = new Uri(Configuration["PokeApiBaseUrl"]);
            });

            serviceCollection.AddHttpClient<IShakespeareTranslatorService, ShakespeareTranslatorService>(options =>
            {
                options.BaseAddress = new Uri(Configuration["FunTranslationsBaseUrl"]);

                options.DefaultRequestHeaders.Add("X-Funtranslations-Api-Secret", Configuration["FunTranslationsApiKey"]);

            });

            ServiceProvider = serviceCollection.BuildServiceProvider();
        }


        private ServiceProvider ServiceProvider { get; set; }
        private IConfiguration Configuration { get; set; }


        [Test]
        [TestCase("Charizard")]
        [TestCase("Abra")]
        [TestCase("Aggron")]
      
        public async Task Get_Pokemon_By_Name(string pokemon)
        {
            using var scope = ServiceProvider.CreateScope();

            var context = scope.ServiceProvider.GetRequiredService<IPokeapiService>();

            var response = await context.GetPokemonInfo(pokemon);

            Assert.IsNotNull(response);
        }

        [Test]
        [TestCase("Malenia")]
        [TestCase("Margit")]
        [TestCase("Thrall")]
        public async Task Detects_Invalid_Pokemon(string pokemon)
        {
            using var scope = ServiceProvider.CreateScope();

            var context = scope.ServiceProvider.GetRequiredService<IPokeapiService>();

            var response = await context.GetPokemonInfo(pokemon);

            Assert.IsNull(response);
        }


        [Test]
        [TestCase(7)]
        [TestCase(14)]
        [TestCase(13)]
        public async Task Get_Pokemon_Speciies_By_Id(int id)
        {
            using var scope = ServiceProvider.CreateScope();

            var context = scope.ServiceProvider.GetRequiredService<IPokeapiService>();

            var response = await context.GetSpeciesInfo(id);

            Assert.IsNotNull(response);
        }

        [Test]
        [TestCase("All tapes left in a car for more than about a fortnight metamorphose into Best of Queen albums.")]
        [TestCase("Always remember that the crowd that applauds your coronation is the same crowd that will applaud your beheading. People like a show.")]
        [TestCase("The pen is mightier than the sword� if the sword is very small and the pen is very sharp.")]
        public async Task Get_Translate_Text_To_Shakespear(string text)
        {
            using var scope = ServiceProvider.CreateScope();

            var context = scope.ServiceProvider.GetRequiredService<IShakespeareTranslatorService>();

            var response = await context.TranslateDescription(text);

            Assert.IsNotNull(response);
        }


        [Test]
        [TestCase("Litten")]
        [TestCase("Steelix")]
        [TestCase("Mudsdale")]
        public async Task Get_Pokemon_With_Translation(string pokemon)
        {
            using var scope = ServiceProvider.CreateScope();

            var context = scope.ServiceProvider.GetRequiredService<IPokemonSearchService>();

            var response = await context.SearchPokemon(pokemon);            

            Assert.Multiple(() =>
            {
                Assert.IsNotNull(response);
                Assert.IsNotNull(response.Name);
                Assert.IsNotNull(response.Description);
                Assert.IsNotNull(response.Sprite);
            });
        }



        [Test]
        [TestCase("Litten")]
        [TestCase("Steelix")]
        [TestCase("Mudsdale")]
        public async Task Get_Pokemon_MockVersion(string pokemon)
        {
            using var scope = ServiceProvider.CreateScope();

            var context = scope.ServiceProvider.GetRequiredService<IPokemonSearchService>();

            var controller = new PokemonController(context);
         
            var actionResult = await controller.GetPokenmon(pokemon);

            Assert.IsInstanceOf<OkObjectResult>(actionResult);
        }
    }
}