/// <reference types="cypress" />

// Welcome to Cypress!
//
// This spec file contains a variety of sample tests
// for a todo list app that are designed to demonstrate
// the power of writing tests in Cypress.
//
// To learn more about how Cypress works and
// what makes it such an awesome testing tool,
// please read our getting started guide:
// https://on.cypress.io/introduction-to-cypress

describe("example to-do app", () => {
  beforeEach(() => {
    // Cypress starts out with a blank slate for each test
    // so we must tell it to visit our website with the `cy.visit()` command.
    // Since we want to visit the same URL at the start of all our tests,
    // we include it in our beforeEach function so that it runs before each test
    cy.visit("https://localhost:44411/");
  });

  it("search for pokemon", () => {
    cy.get('input[type="text"]').type("charizard{enter}");

    cy.get("h1").first().should("have.text", "charizard");
  });

  it("clear and research", () => {
    cy.get('input[type="text"]').type("charizard{enter}");

    cy.get("#back_button").click();

    cy.get('input[type="text"]').type("pikachu{enter}");

    cy.get("h1").first().should("have.text", "pikachu");
  });


  it('error message diplsya', () => {

    cy.get('input[type="text"]').type("notapokimon{enter}");

    cy.get("#error-message").first().should('be.visible');


  })
});
