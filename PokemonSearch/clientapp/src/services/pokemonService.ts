import { Pokemon } from "../types";

export const getPokemon = async (name: string): Promise<Pokemon> => {
    const response = await fetch(`/api/pokemon/${name}`)
        .then(response => {
            if (!response.ok) {
                throw new Error(response.statusText)
            }
            return response.json();
        })

    return <Pokemon>response;
}