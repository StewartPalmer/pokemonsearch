import { ChangeEvent, Component, FormEvent, FunctionComponent } from "react";

type SearchProps = {
  setSearch: React.Dispatch<React.SetStateAction<string>>;
  showErrorMessage: boolean,
};

type SearchState = {
  search: string;

};
export class SearchComponent extends Component<SearchProps, SearchState> {
  constructor(props: SearchProps) {
    super(props);

    this.state = {
      search: "",
     
    };
  }

  handleChange(e: ChangeEvent<HTMLInputElement>) {
    this.setState({ search: e.target.value });
  }

  sumbitSearch(e: FormEvent<HTMLFormElement>) {

    e.preventDefault();

    this.props.setSearch(this.state.search);
  }

  render() {
    return (
   
        <form aria-label="search for a pokemon" className="text-gray-700 flex flex-col w-full items-center md:items-start" onSubmit={(e) => this.sumbitSearch(e) }>
          <label className="font-press-start mb-4 md:mb-auto">Search For A Pokemon</label>
          <div className="flex items-center flex-col md:flex-row">
            <input
              type="text"
              placeholder="Enter A Pokemon Name"
              className="bg-gray-100 font-press-start p-4"
              onChange={(event) => this.handleChange(event)}
            ></input>
            <button type="submit" id="search-button" className="bg-green-400 font-press-start rounded-full h-16 w-16 flex justify-center items-center text-center ml-4 hover:bg-green-300 text-xs mt-4 md:mt-auto">
              GO
            </button>
          </div>
          { this.props.showErrorMessage ?  <div id='error-message' className="font-press-start text-red-600 text-xs mt-4 text-center md:text-left">No Such Pokemon, Please try another name</div> : null}
         
        </form>
   
    );
  }
}
