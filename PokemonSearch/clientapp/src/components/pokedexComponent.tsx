import { FunctionComponent } from "react";
import { Pokemon } from "../types";

type PokedexProps = {
  name: string;
  sprite: string;
  description: string;
  setPokemon: React.Dispatch<React.SetStateAction<Pokemon | undefined>>;
};

// we can use children even though we haven't defined them in our CardProps
export const PokidexComponent: FunctionComponent<PokedexProps> = ({
  name,
  sprite,
  description,
  setPokemon,
}) => (
  <div className="bg-red-700  w-full drop-shadow-2xl rounded-lg  z-10">
    <div className="m-4 border-solid border-8 border-red-900 flex rounded flex-col">
      <div className="m-4 bg-gray-400 flex rounded-tl-lg  rounded-br-lg  drop-shadow-2xl">
        <div className="m-8 bg-white flex w-full shadow-inner rounded  aspect-square overflow-scroll md:overflow-auto">
          <div className="flex w-full m-4 flex-col">
          <img
            className="aspect-square object-contain h-32"
            src={sprite}
          ></img>
          <div className="font-press-start">
          <h1 className="uppercase mb-4">{name}</h1>
          <p className="text-xs">{description}</p>
          </div>
          </div>
        </div>
      </div>
      <div>
        <div className="flex m-4 justify-between h-40">
          <div className="">
            <div className="h-20 w-20 rounded-full bg-gray-700 drop-shadow-2xl"></div>
          </div>
          <div className="flex ">
            <button className="h-14 w-14 rounded-full bg-blue-500 mr-4 mt-12 drop-shadow-2xl font-press-start text-xs" id="back_button" onClick={() => setPokemon(undefined)}> Back</button>
            <button className="h-14 w-14 rounded-full bg-green-500 drop-shadow-2xl font-press-start text-xs" > A </button>
          </div>
        </div>
      </div>
    </div>
  </div>
);
