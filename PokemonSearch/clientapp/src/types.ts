export interface Pokemon {
  name: string;
  description: string;
  sprite: string;
}
