import { render, screen } from '@testing-library/react';
import App from './App';
import ReactDOM from 'react-dom';




it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});





test('check search area is present on load', () => {

  render(<App />);

  const searchButton = screen.getByText(/go/i);

  const searchInput = screen.getByPlaceholderText(/Enter A Pokemon Name/i)  

  expect(searchButton).toBeInTheDocument();

  expect(searchInput).toBeInTheDocument();
});



