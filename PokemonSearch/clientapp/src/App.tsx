import React, { useEffect, useState } from "react";
import "./App.css";
import { SearchComponent } from "./components/searchComponent";
import { PokidexComponent } from "./components/pokedexComponent";
import { getPokemon } from "./services/pokemonService";
import { Pokemon } from "./types";

function App() {
  const [search, setSearch] = useState<string>("");

  const [pokemon, setPokemon] = useState<Pokemon | undefined>();

  const [notFound, setNotFound] = useState<boolean>(false);

  useEffect(() => {
    setNotFound(false);
    if (search === "") return;

    getPokemon(search)
      .then((response) => {
        setPokemon(response);
        setSearch("");
      })
      .catch((error) => {
        setNotFound(true);
      });
  }, [search]);

  const renderContent = () => {
    if (pokemon !== undefined)
      return (
        <PokidexComponent
          setPokemon={setPokemon}
          name={pokemon.name}
          description={pokemon.description}
          sprite={pokemon.sprite}
        />
      );

    return <SearchComponent setSearch={setSearch}  showErrorMessage={notFound}/>;
  };

  return (
    <div className="bg-white flex justify-center items-center w-full h-screen">
      <div className="flex w-full justify-center items-center md:max-w-md flex-col">
        <div className="flex w-full">{renderContent()}</div>
      </div>
    </div>
  );
}

export default App;
