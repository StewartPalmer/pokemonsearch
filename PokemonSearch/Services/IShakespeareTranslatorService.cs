﻿namespace PokemonSearch.Services
{
    public interface IShakespeareTranslatorService
    {
        Task<string> TranslateDescription(string text);
    }
}