﻿using PokemonSearch.Models.ResponseEntites;

namespace PokemonSearch.Services
{
    public interface IPokemonSearchService
    {
        Task<SearchResponse?> SearchPokemon(string name);
    }
}