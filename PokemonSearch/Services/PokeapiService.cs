﻿using PokemonSearch.Models.Pokeapi;

namespace PokemonSearch.Services
{
    public class PokeapiService : IPokeapiService
    {
        private readonly HttpClient _httpClient;

        public PokeapiService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<PokemonResponse?> GetPokemonInfo(string name)
        {

            var response = await _httpClient.GetAsync($"api/v2/pokemon/{name.ToLower().Trim()}");


            if (response.IsSuccessStatusCode) return await response.Content.ReadFromJsonAsync<PokemonResponse>();

            
            return null;

        }

        public  async Task<PokemonSpeciesResponse> GetSpeciesInfo(int id)
        {

            var response = await _httpClient.GetAsync($"api/v2/pokemon-species/{id}");


            if (response.IsSuccessStatusCode) return await response.Content.ReadFromJsonAsync<PokemonSpeciesResponse>();


            return null;
        }
    }
}
