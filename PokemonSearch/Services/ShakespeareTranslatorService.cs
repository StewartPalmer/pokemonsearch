﻿using PokemonSearch.Models.RequestEntites;
using PokemonSearch.Models.ResponseEntites;
using System.Web;

namespace PokemonSearch.Services
{
    public class ShakespeareTranslatorService : IShakespeareTranslatorService
    {
        private readonly HttpClient _httpClient;

        public ShakespeareTranslatorService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<string> TranslateDescription(string text)
        {

            var content = new TranslateDescriptionRequest
            {
                Text = text
            };

            var response = await _httpClient.PostAsJsonAsync($"translate/shakespeare.json?", content);


            if (response.IsSuccessStatusCode)
            {
                var model = await response.Content.ReadFromJsonAsync<TranslateDescriptionResponse>();


                return model.Contents.Translated;
            };


            return null;
        }
    }
}
