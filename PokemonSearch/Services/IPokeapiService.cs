﻿namespace PokemonSearch.Services
{
    public interface IPokeapiService
    {
        Task<PokemonResponse?> GetPokemonInfo(string name);
        Task<Models.Pokeapi.PokemonSpeciesResponse> GetSpeciesInfo(int id);
    }
}