﻿using PokemonSearch.Models.ResponseEntites;

namespace PokemonSearch.Services
{
    public class PokemonSearchService : IPokemonSearchService
    {
        private readonly IPokeapiService _pokeapiService;
        private readonly IShakespeareTranslatorService _shakespeareTranslatorService;

        public PokemonSearchService(IPokeapiService pokeapiService, IShakespeareTranslatorService shakespeareTranslatorService)
        {
            _pokeapiService = pokeapiService;
            _shakespeareTranslatorService = shakespeareTranslatorService;
        }

        public async Task<SearchResponse?> SearchPokemon(string name)
        {
            var pokemon = await _pokeapiService.GetPokemonInfo(name);


            if (pokemon == null) return null;

            var species = await _pokeapiService.GetSpeciesInfo(pokemon.Id);

            var flavourtext = species.FlavorTextEntries.FirstOrDefault(x => x.Language.Name.Equals("en", StringComparison.InvariantCultureIgnoreCase));

            if (flavourtext == null) return null;

            var translatedDescrpiton = await _shakespeareTranslatorService.TranslateDescription(flavourtext.FlavorText);

            var response = new SearchResponse
            {
                Name = pokemon.Name,
                Sprite = pokemon.Sprites.FrontDefault,
                Description = translatedDescrpiton ?? flavourtext.FlavorText,

            };


            return response;

        }
    }
}
