using Microsoft.OpenApi.Models;
using PokemonSearch.Services;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllersWithViews();

builder.Services.AddSingleton<IPokeapiService, PokeapiService>();

builder.Services.AddSingleton<IShakespeareTranslatorService, ShakespeareTranslatorService>();

builder.Services.AddScoped<IPokemonSearchService, PokemonSearchService>();

builder.Services.AddHttpClient<IPokeapiService, PokeapiService>(client =>
{
    client.BaseAddress = new Uri(builder.Configuration["PokeApiBaseUrl"]);
});



builder.Services.AddHttpClient<IShakespeareTranslatorService, ShakespeareTranslatorService>(options =>
{
    options.BaseAddress = new Uri(builder.Configuration["FunTranslationsBaseUrl"]);

    options.DefaultRequestHeaders.Add("X-Funtranslations-Api-Secret", builder.Configuration["FunTranslationsApiKey"]);
});

builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo { Title = "Pokemon Search Api", Version = "v1" });
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseStaticFiles();
app.UseRouting();
app.MapControllerRoute(
    name: "default",
    pattern: "{controller}/{action=Index}/{id?}");
app.MapFallbackToFile("index.html"); ;

app.Run();



