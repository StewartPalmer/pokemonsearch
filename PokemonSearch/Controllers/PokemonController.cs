﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PokemonSearch.Services;

namespace PokemonSearch.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PokemonController : ControllerBase
    {

        private readonly IPokemonSearchService _pokemonSearchService;   

        public PokemonController(IPokemonSearchService pokemonSearchService)
        {
            _pokemonSearchService = pokemonSearchService;      
        }

        [HttpGet("{name}")]
        public async Task<IActionResult> GetPokenmon(string name)
        {
            var pokemon = await _pokemonSearchService.SearchPokemon(name);

            if (pokemon == null) return NotFound();

            return Ok(pokemon);
        }
    }
}
