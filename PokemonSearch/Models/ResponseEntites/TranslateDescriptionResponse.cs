﻿using System.Text.Json.Serialization;

namespace PokemonSearch.Models.ResponseEntites
{

    public class Contents
    {
        [JsonPropertyName("translation")]
        public string Translation { get; set; }

        [JsonPropertyName("text")]
        public string Text { get; set; }

        [JsonPropertyName("translated")]
        public string Translated { get; set; }
    }

    public class TranslateDescriptionResponse

    {
        [JsonPropertyName("success")]
        public Success Success { get; set; }

        [JsonPropertyName("contents")]
        public Contents Contents { get; set; }
    }

    public class Success
    {
        [JsonPropertyName("total")]
        public int Total { get; set; }
    }
}
