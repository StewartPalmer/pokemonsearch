﻿namespace PokemonSearch.Models.ResponseEntites
{
    public class SearchResponse
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public string Sprite { get; set; }
    }
}
